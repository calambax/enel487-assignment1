The Program:
The inculded assignment1.cpp contains a program  which asks the user for an operation (A/a for addition, 
S/s for subtraction, M/m for multiplication, and D/d for division) followed by 4 consecutive floating point
numbers for the x + jx and y+jy values of complex numbers. The program does not allow characters outside Q/q 
and the corresponding characters for mathematical operations. If an invalid input is given, such as 'i', 
the program will tell the user that the input is invalid and must try again. one of the limitations of this program
is that I have not tested it of the user inputs a string. However, I am assuming that the program will register the 
first character of the string. Depending on the operation selected by the user, it will output the desired operation
after the input is given. On the interactive version, the selected operation is also printed before the answer, as 
well as the parameters the user has given.

Testing the Program:
The IDE used in writing the program is QT Creator with g++ 4.9.2 compiler in ubuntu. The following commands were used
during testing: 
g++ -o assign1 assignment1.cpp
./assign1 < data.txt > output.txt

The resulting output.txt is included in the submission.

Also, the QT Creator created a Makefile. However, I did not include it on the submission because I have not written
it myself.