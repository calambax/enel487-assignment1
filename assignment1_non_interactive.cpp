/*
 Project: Assignment 1 - Simple Complex Number Calculator
 Version: Non-interactive
 Author: Xiam Calamba
 Course: ENEL 487
 Date: September 29, 2015
 Description: The program asks for user's desired operation and inputs the 
 complex numbers he/she wants to be processed. The program will the give the
 answer to the user. The program is in a flag-controlled loop and will exit 
 if the user inputs 'Q' or 'q'.
 
 */




#include<iostream>
#include<cstdio>
#include<cmath>

using namespace std;

#define PI 3.14159265

float mulRealAns(float a_real, float a_img, float b_real, float b_img);
float mulImgAns(float a_real, float a_img, float b_real, float b_img);
float divRealAns(float a_real, float a_img, float b_real, float b_img);
float divImgAns(float a_real, float a_img, float b_real, float b_img);
char checkImgSign(float ans_img);
void printAns(float ans_real, float ans_img);


int main()
{
    float ans_real, ans_img;
    float a_real, a_img, b_real, b_img;
    bool done = 0;
    char user_in, img_sign;

    while (!done)
    {

        cin >> user_in;
        if ((user_in == 'q') || (user_in == 'Q'))
        {
            done = 1;
        }
        else
        {
            if ((user_in == 'a') || (user_in == 'A'))
            {
                cin >> a_real >> a_img >> b_real >> b_img;
                ans_real = a_real + b_real;
                ans_img = a_img + b_img;
                printAns(ans_real,ans_img);

            }
            else if ((user_in == 's') || (user_in == 'S'))
            {
                cin >> a_real >> a_img >> b_real >> b_img;
                ans_real = a_real - b_real;
                ans_img = a_img - b_img;
                printAns(ans_real,ans_img);

            }
            else if ((user_in == 'm') || (user_in == 'M'))
            {
                cin >> a_real >> a_img >> b_real >> b_img;
                ans_real = mulRealAns(a_real, a_img,b_real, b_img);
                ans_img = mulImgAns(a_real, a_img,b_real, b_img);
                printAns(ans_real,ans_img);

            }
            else if ((user_in == 'd') || (user_in == 'D'))
            {
                cin >> a_real >> a_img >> b_real >> b_img;
                ans_real = divRealAns(a_real, a_img,b_real, b_img);
                ans_img = divImgAns(a_real, a_img,b_real, b_img);
                printAns(ans_real,ans_img);

            }
            else
            {
                stderr << "The input" << user_in << " does not correspond to a valid arithmetic operator input." << endl;
                stderr << "Please try again." << endl;
            }


        }
    }
    return 0;
}

void printAns(float ans_real, float ans_img)
{
    char img_sign;
    img_sign = checkImgSign(ans_img);
    float abs_ans_img;
    abs_ans_img = abs(ans_img);
    stdout << ans_real << " " << img_sign << " j " << abs_ans_img << endl;
}

char checkImgSign(float ans_img)
{
    char img_sign;
    if (ans_img < 0)
    {
        img_sign = '-';
    }
    else
    {
        img_sign = '+';
    }
    return img_sign;
}

float mulRealAns(float a_real, float a_img, float b_real, float b_img)
{
    float a_polar, b_polar, polar_product, a_angle, b_angle, polar_angle,ans_real;
    a_polar = sqrt((a_real*a_real)+(a_img*a_img));
    b_polar = sqrt((b_real*b_real)+(b_img*b_img));
    polar_product = a_polar*b_polar;
    a_angle = (atan(a_img/a_real) * 180) / PI;
    b_angle = (atan(b_img/b_real) * 180) / PI;
    polar_angle = a_angle+b_angle;
    ans_real = (cos(polar_angle * PI / 180)) * polar_product;
    return ans_real;
}

float mulImgAns(float a_real, float a_img, float b_real, float b_img)
{
    float a_polar, b_polar, polar_product, a_angle, b_angle, polar_angle, ans_img;
    a_polar = sqrt((a_real*a_real)+(a_img*a_img));
    b_polar = sqrt((b_real*b_real)+(b_img*b_img));
    polar_product = a_polar*b_polar;
    a_angle = (atan(a_img/a_real) * 180) / PI;
    b_angle = (atan(b_img/b_real) * 180) / PI;
    polar_angle = a_angle+b_angle;
    ans_img = (sin(polar_angle * PI /180)) * polar_product;
    return ans_img;
}

float divRealAns(float a_real, float a_img, float b_real, float b_img)
{
    float a_polar, b_polar, polar_product, a_angle, b_angle, polar_angle, ans_real;
    a_polar = sqrt((a_real*a_real)+(a_img*a_img));
    b_polar = sqrt((b_real*b_real)+(b_img*b_img));
    polar_product = a_polar/b_polar;
    a_angle = atan(a_img/a_real) * 180 / PI;
    b_angle = atan(b_img/b_real) * 180 / PI;
    polar_angle = a_angle-b_angle;
    ans_real = (cos(polar_angle * PI / 180)) * polar_product;
    return ans_real;
}

float divImgAns(float a_real, float a_img, float b_real, float b_img)
{
    float a_polar, b_polar, polar_product, a_angle, b_angle, polar_angle, ans_img;
    a_polar = sqrt((a_real*a_real)+(a_img*a_img));
    b_polar = sqrt((b_real*b_real)+(b_img*b_img));
    polar_product = a_polar/b_polar;
    a_angle = atan(a_img/a_real) * 180 / PI;
    b_angle = atan(b_img/b_real) * 180 / PI;
    polar_angle = a_angle-b_angle;
    ans_img = (sin(polar_angle * PI /180)) * polar_product;
    return ans_img;

}

